<!DOCTYPE html>
<html lang="en">
  <head>
  	<title>Aplikasi Pengelolaan Laundry</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="content/assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="content/assets/login/css/style.css">

	</head>
	<body class="img js-fullheight" style="background-image: url(content/assets/login/images/bg.jpg);">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Aplikasi Pengelolaan Laundry</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
				  <div class="login-wrap p-0">
		      		<h3 class="mb-4 text-center">Sudah Punya Akun?</h3>
		      		<form method="POST" action="ceklogin.php" class="signin-form">
							<?php if (isset($_GET['msg'])): ?>
								<h3 class="text-danger col-md-12"><span class="badge badge-danger"><?= $_GET['msg'];  ?></span></h3>
							<?php endif ?>
				      	<div class="form-group">
				      		<input type="text" name="username" class="form-control" placeholder="Username" data-validate="Username is required" required>
				      	</div>
			            <div class="form-group">
			              	<input id="password-field" name="password" type="password" class="form-control" placeholder="Password" data-validate = "Password is required" required>
			              	<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
			            </div>
			            <div class="form-group">
			            	<button type="submit" class="form-control btn btn-primary submit px-3">Sign In</button>
			            </div>
			            <div class="form-group d-md-flex">
			            	<div class="w-50">
				            	<label class="checkbox-wrap checkbox-primary">Remember Me
									<input type="checkbox" checked>
										<span class="checkmark"></span>
								</label>
							</div>
							<div class="w-50 text-md-right">
								<a href="#" style="color: #fff">Lupa Password ?</a>
							</div>
			            </div>
		          	</form>
			      </div>
				</div>
			</div>
		</div>
	</section>

  <script src="content/assets/login/js/jquery.min.js"></script>
  <script src="content/assets/login/js/popper.js"></script>
  <script src="content/assets/login/js/bootstrap.min.js"></script>
  <script src="content/assets/login/js/main.js"></script>
</body>
</html>